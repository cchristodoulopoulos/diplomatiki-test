__aspect_classes = []
__aspect_binds = []
__beans = []
__around_rets = {}


class AspectBinds:
    def __init__(self, method, function, when):
        self.method = method
        self.function = function
        self.when = when

    def __str__(self):
        return self.method + ' / ' + self.function.__name__ + ' / ' + self.when


def aspect(cls):
    __aspect_classes.append(cls)


def before(method):
    def _before(function):
        __aspect_binds.append(AspectBinds(method, function, 'before'))

    return _before


def after(method):
    def _before(function):
        __aspect_binds.append(AspectBinds(method, function, 'after'))

    return _before


def around(method):
    def _around(function):
        __aspect_binds.append(AspectBinds(method, function, 'around'))

    return _around


def compose_before(f, asp):
    def wr(*args, **kwargs):
        asp(None, *args, **kwargs)
        return f(*args, **kwargs)

    return wr


def compose_after(f, asp):
    def wr(*args, **kwargs):
        ret = f(*args, **kwargs)
        kwargs['ret'] = ret
        asp(None, *args, **kwargs)
        return ret

    return wr


def compose_around(f, asp):
    def wr(*args, **kwargs):
        def __proceed__():
            __around_rets[args[0]] = f(*args, **kwargs)

        setattr(args[0], 'proceed', __proceed__)

        asp(None, *args, **kwargs)
        return __around_rets[args[0]]

    return wr


def apply_aspects():
    for a in __aspect_binds:
        class_name = a.method.split('.')[0]
        method_name = a.method.split('.')[1]
        class_object = None
        for i in __beans:
            if class_name == i[0]:
                class_object = i[1]
                break
        if a.when == 'before':
            setattr(class_object, method_name, compose_before(getattr(class_object, method_name), a.function))
        elif a.when == 'after':
            setattr(class_object, method_name, compose_after(getattr(class_object, method_name), a.function))
        elif a.when == 'around':
            setattr(class_object, method_name, compose_around(getattr(class_object, method_name), a.function))


def find_beans(module):
    import inspect, sys
    classes = inspect.getmembers(sys.modules[module.__name__], inspect.isclass)
    __beans.extend(classes)


'''
def decorator(argument):
    def real_decorator(function):
        def wrapper(*args, **kwargs):
            funny_stuff()
            something_with_argument(argument)
            function(*args, **kwargs)
            more_funny_stuff()
        return wrapper
     return real_decorator
'''
