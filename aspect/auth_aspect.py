from framework import framework


@framework.aspect
class AuthAspect:
    @framework.before(method='AuthManager.login')
    def before_login(self,*args):
        print("action before", args[1],args[2])

    @framework.after(method='AuthManager.login')
    def after_login(self,*args, **kwargs):
        print("action after", args[1],args[2], kwargs)

    @framework.around(method='AuthManager.login')
    def around_login(self,*args, **kwargs):
        print('eimai to auth aspect around')
        args[0].proceed()
        print('eimai to auth aspect around333')
