from aspect import auth_aspect
from business_logic import auth_manager
from framework import framework

# config

# prepei na doso to "directory" me tis klaseis pou tha kano intercept
framework.find_beans(auth_manager)

framework.apply_aspects()


p = auth_manager.AuthManager()
# print('eimai to instance', p)
print('e', p.login('bober', 'bober'))
print('e', p.login('bober', 'loa'))
# print('e', p.login('bober', 'bober'))
# print('e', p.login('bober', 'bober'))


